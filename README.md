# N-Version Training
Dieses Repository implementiert die Trainingsroutinen für das Attention Unet und das R2Unet wie sie in der Arbeit "Implementierung eines N-Version Systems zur
Schienenerkennung auf Basis neuronaler Netze in ROS" beschrieben wird. Es wird dazu das Paket [keras-unet-collection](https://github.com/yingkaisha/keras-unet-collection) von Sha, Yingkai verwendet. Es wurde dabei für die Netze eine Option hinzugefügt, die Dropouts mit einbaut.


## Installation
Die Conda-Umgebung für das Training kann aus der environment.yml erstellt werden:

    conda env create -f environment.yml

## Datensatz
Um den Datensatz in Trainings-, Validierungs- und Testdaten zu trennen, kann das Skript `split_dataset` verwendet werden:

    python split_dataset.py --save_path training_data/ --dataset pfad/zu/rs19_val/

Dieser Aufruf teilt den Testdatensatz Railsem19 in 70% Trainingsdaten, 20% Validierungsdaten und 10% Testdaten auf. Um andere Aufteilungen zu verwenden kann die Aufteilung mit --train_size, --valid_size und --test_size bestimmt werden.


## Training
Sobald der Trainings- und Validierungsdatensatz erstellt worden sind kann das Training gestartet werden. Für das Training wurden jeweils zwei verschiedene Ansätze implementiert:

Beim ersten Ansatz werden die Netze mit der Funktion fit() trainiert. Die Skripte dazu sind `attunet.py` und `r2unet.py`.

Beim zweiten Ansatz werden die Netze mit der einer eigenen Trainingsfunktion trainiert. In dieser Funktion werden die Gradienten über mehrere Steps aufakkumuliert. Das Training ist in den Skripten `attunet_acc.py` und `r2unet_acc.py` implementiert. 

Mit den sbatch-Dateien `attunet.sbatch` und `r2unet.sbatch` können die Trainingsroutinen jeweils gestartet werden. Die Parameter werden innerhalb der Skripte definiert.


## Tests
Um die Netze zu testen wurde das Skript `nn_tester.py` geschrieben, welches die Vorhersagen eines Modells über alle Daten hinweg aufnimmt, dessen Metriken berechnet und sie mittelt. Zu den Metriken gehören f1_score, jaccard_score, accuracy_score, precision_score und der recall_score.