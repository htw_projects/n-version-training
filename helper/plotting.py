import matplotlib.pyplot as plt

def plot_history(loss, val_loss, accuracy, val_accuracy, save_pth):
    fig, ax = plt.subplots(1, 2, figsize=(12, 4))

    # plot loss
    ax[0].plot(loss, label='Training loss')
    ax[0].plot(val_loss, label='Validation loss')
    ax[0].set_title('Training and Validation Loss')
    ax[0].set_xlabel('Epochs')
    ax[0].set_ylabel('Loss')
    ax[0].legend()

    # plot accuracy (if available)
    if accuracy is not None and val_accuracy is not None:
        ax[1].plot(accuracy, label='Training Accuracy')
        ax[1].plot(val_accuracy, label='Validation Accuracy')
        ax[1].set_title('Training and Validation Accuracy')
        ax[1].set_xlabel('Epochs')
        ax[1].set_ylabel('Accuracy')
        ax[1].legend()

    plt.savefig(save_pth)
