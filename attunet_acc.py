import os
import time
import numpy as np
from glob import glob
from helper.plotting import plot_history

import tensorflow as tf
from collection.keras_unet_collection import utils, models

filepath = 'training_data/'
model_path = 'models/'
checkpoints_path = 'checkpoints/'

height = 1056
width = 1920
N_labels = 2  # number of labels in GT

N_epoch = 400 # number of epoches
steps = None # number of batches per epoch in training
batch = [4,4] # number of samples per batch for [training, validation]
accumulation_steps = 8  # number of accumulations before updating the model

patience = 20 # the max-allowed early stopping patience
min_delta = 0 # the lowest acceptable loss value reduction


loss_hist = []
loss_valid_hist = []
accuracy_hist = []
accuracy_valid_hist = []
accuracy = tf.keras.metrics.Accuracy()
accuracy_valid = tf.keras.metrics.Accuracy()


def target_data_process(target_array, num_classes=2):
    return utils.to_categorical(target_array, num_classes=num_classes)

target_size = (height,width)
def load_image(image_path):
    image = tf.io.read_file(image_path)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, target_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    return image

def load_mask(mask_path, num_classes):
    mask = tf.io.read_file(mask_path)
    mask = tf.image.decode_png(mask, channels=1)
    mask = tf.image.convert_image_dtype(mask, tf.int32)
    mask = tf.image.resize(mask, target_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    mask = tf.one_hot(mask, num_classes)
    mask = tf.squeeze(mask)
    return mask

def process_path(image_path, mask_path):
    image = load_image(image_path)
    mask = load_mask(mask_path, 2)
    return image, mask


# file path after data extraction
dataset=[]
for idx, data in enumerate(['train', 'valid']):#, 'test']):
    input_names = np.array(sorted(glob(filepath+data+'/*.jpg')))
    label_names = np.array(sorted(glob(filepath+data+'_GT/*.png')))

    dataset.append(tf.data.Dataset.from_tensor_slices((input_names, label_names)))
    dataset[-1] = dataset[-1].map(process_path, num_parallel_calls=tf.data.AUTOTUNE)
    dataset[-1] = dataset[-1].batch(batch[idx])
dataset[0] = dataset[0].shuffle(300)

model = models.att_unet_2d((height, width, 3), filter_num=[64, 128, 256, 512, 1024], 
                        dropout=[0.5, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.5], n_labels=N_labels,
                        stack_num_down=2, stack_num_up=2, activation='ReLU',
                        atten_activation='ReLU', attention='add', output_activation='Sigmoid',
                        batch_norm=True, pool=True, unpool=True,
                        backbone='VGG16', #weights='imagenet',
                        freeze_backbone=False, freeze_batch_norm=False,
                        name='attunet')

optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)
loss_fn = tf.keras.losses.binary_crossentropy

@tf.function
def train_step(images, labels):
    with tf.GradientTape() as tape:
        predictions = model(images, training=True)
        loss = tf.reduce_mean(loss_fn(labels, predictions))
        accuracy.update_state(tf.argmax(labels, axis=-1), tf.argmax(predictions, axis=-1))
    gradients = tape.gradient(loss, model.trainable_variables)
    return gradients, loss


# loop over epoches
print(f"Start training of {model.name} with input {model.input_shape}, output {model.output_shape}, {N_epoch} epochs and a batchsize of {batch[0]}x{accumulation_steps}")

# training loop
valid_loss_min = 0.0
patience_cnt = 0
epoch = 0
for epoch in range(N_epoch):
    t_start = time.time()
    accumulated_gradients = [tf.zeros_like(var) for var in model.trainable_variables]
    loss_total = 0.0
    accuracy.reset_states()
    accuracy_valid.reset_states()
    batch_cnt = 0

    # training
    for step, (images, labels) in enumerate(dataset[0]):
        gradients, loss = train_step(images, labels)
        loss_total += loss
        batch_cnt += 1

        accumulated_gradients = [acc_grad + grad for acc_grad, grad in zip(accumulated_gradients, gradients)]

        epoch_complete=(steps is None and (step + 1) == len(dataset[0]))\
                    or (steps is not None and (step + 1) == steps)

        # update weights
        if (step + 1) % accumulation_steps == 0 or epoch_complete:
            optimizer.apply_gradients(zip(accumulated_gradients, model.trainable_variables))
            accumulated_gradients = [tf.zeros_like(var) for var in model.trainable_variables]
        if epoch_complete:
            break
    loss_total /= accumulation_steps * batch_cnt

    # validation
    valid_loss = 0.0
    batch_cnt = 0
    for images, labels in dataset[1]:
        predictions = model(images, training=False)
        v_loss = tf.reduce_mean(loss_fn(labels, predictions))
        valid_loss += v_loss
        accuracy_valid.update_state(tf.argmax(labels, axis=-1), tf.argmax(predictions, axis=-1))
        batch_cnt += 1
    valid_loss /= batch_cnt
   
    # update history:
    loss_hist.append(loss_total)
    loss_valid_hist.append(valid_loss)
    accuracy_hist.append(accuracy.result())
    accuracy_valid_hist.append(accuracy_valid.result())

    # early stopping:
    if (valid_loss - valid_loss_min) < min_delta or valid_loss_min == 0:
        valid_loss_min = valid_loss
        patience_cnt = 0
    else:
        patience_cnt += 1
        if patience_cnt > patience:
            print("Early stopping at epoch %04i, loss: %.4f, valid loss: %.4f" % (epoch, loss_total, valid_loss))
            break

    # checkpoints:
    if checkpoints_path is not None:
        pth = os.path.join(checkpoints_path, model.name + "." + str(epoch))
        model.save_weights(pth)

    print("[%is]epoch %04i, loss: %.4f, valid loss: %.4f" % (int(time.time() - t_start), epoch, loss_total, valid_loss))

# save plot and model:
print("training complete! Saving data...")
model_name = f"{model.name}_{model.input_shape[2]}x{model.input_shape[1]}_epoch_{epoch}_batchsize_{batch[0]}x{accumulation_steps}"
if model_path!= None:
    model_name = os.path.join(model_path, model_name)
model.save(model_name + ".hdf5")
plot_history(loss_hist, loss_valid_hist, 
             accuracy_hist, accuracy_valid_hist,
             model_name + ".png")
print("data saved")