import os
import numpy as np
from glob import glob
from helper.plotting import plot_history

import tensorflow as tf
from tensorflow import keras
from collection.keras_unet_collection import utils, models
from keras.callbacks import Callback, EarlyStopping

filepath = 'training_data/'
model_path = 'models/'
checkpoints_path = 'checkpoints/'

height = 1056#720#576#576#1056#864
width = 1920#1280#960#1024#1920#1536
N_labels = 2  # number of labels in GT

N_epoch = 400 # number of epoches
steps = [None,None] # number of batches per epoch in [training, validation]
batch = [2,2] # number of samples per batch for [training, validation]

patience = 10 # the max-allowed early stopping patience
min_delta = 0 # the lowest acceptable loss value reduction


def target_data_process(target_array, num_classes=2):
    return utils.to_categorical(target_array, num_classes=num_classes)

target_size = (height,width)
def load_image(image_path):
    image = tf.io.read_file(image_path)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, target_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    return image

def load_mask(mask_path, num_classes):
    mask = tf.io.read_file(mask_path)
    mask = tf.image.decode_png(mask, channels=1)
    mask = tf.image.convert_image_dtype(mask, tf.int32)
    mask = tf.image.resize(mask, target_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    mask = tf.one_hot(mask, num_classes)
    mask = tf.squeeze(mask)
    return mask

def process_path(image_path, mask_path):
    image = load_image(image_path)
    mask = load_mask(mask_path, 2)
    return image, mask

class CheckpointsCallback(Callback):
    def __init__(self, checkpoints_path, model_name):
        self.checkpoints_path = checkpoints_path
        self.model_name = model_name

    def on_epoch_end(self, epoch, logs=None):
        if self.checkpoints_path is not None:
            pth = os.path.join(self.checkpoints_path, self.model_name + "." + str(epoch))
            self.model.save_weights(pth)

# file path after data extraction
dataset=[]
for idx, data in enumerate(['train', 'valid']):#, 'test']):
    input_names = np.array(sorted(glob(filepath+data+'/*.jpg')))
    label_names = np.array(sorted(glob(filepath+data+'_GT/*.png')))

    dataset.append(tf.data.Dataset.from_tensor_slices((input_names, label_names)))
    dataset[-1] = dataset[-1].map(process_path, num_parallel_calls=tf.data.AUTOTUNE)
    dataset[-1] = dataset[-1].batch(batch[idx])

print("datasets loaded")

model = models.r2_unet_2d((height, width, 3), [32, 64, 128, 256, 512, 1024], 
                          dropout=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], n_labels=N_labels,
                          stack_num_down=2, stack_num_up=2, recur_num=2,
                          activation='ReLU', output_activation='Sigmoid', 
                          batch_norm=True, pool=True, unpool=True, name='r2unet')

model.compile(loss=keras.losses.binary_crossentropy, optimizer=keras.optimizers.Adam(learning_rate=1e-3), metrics=['accuracy'])
callbacks = [EarlyStopping(monitor='val_loss', min_delta=min_delta, patience=patience, verbose=2, mode='min')]
if checkpoints_path != None:
    callbacks.append(CheckpointsCallback(checkpoints_path, model.name))

# loop over epoches
print(f"Start training of {model.name} with size {model.input_shape}, {N_epoch} epochs and a batchsize of {batch[0]}")

# initial loss record
initial_loss = model.evaluate(dataset[1], verbose=2)
print("Initial val_loss: ", initial_loss)

hist = model.fit(dataset[0],
                 steps_per_epoch=steps[0],
                 validation_data=dataset[1],
                 validation_steps=steps[1],
                 epochs=N_epoch, callbacks=callbacks,
                 verbose=2)

# epoch-end validation
final_loss = hist.history['val_loss'][-1]
print("Final val_loss: ", final_loss)


# save plot and model:
epoch_cnt = len(hist.history['loss'])
model_name = f"{model.name}_{model.input_shape[2]}x{model.input_shape[1]}_epoch_{epoch_cnt}_batchsize_{batch[0]}"
if model_path!= None:
    model_name = os.path.join(model_path, model_name)
model.save(model_name + ".hdf5")
plot_history(hist.history['loss'], hist.history['val_loss'], 
             hist.history.get('accuracy'), hist.history.get('val_accuracy'), 
             model_name + ".png")