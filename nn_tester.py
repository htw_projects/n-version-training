import os
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import f1_score, jaccard_score, accuracy_score, precision_score, recall_score
import itertools
import cv2


dataset_path = 'training_data/'

#modelpath = './models_best/attunet_model_32_to_256_1280x720_epoch_400_batchsize_16_old.hdf5'
modelpath = './models_best/r2unet_model_1280x720_epoch_400_batchsize_8.hdf5'
#modelpath = './models_best/attunet_model_1920x1056_epoch_35_batchsize_4_big_slight_dropout.hdf5'
modelpath = './models_best/attunet_model_1920x1056_epoch_22_batchsize_4x8_drop_0p5x1.hdf5'

#modelpath = './models/r2unet_model_1920x1056_epoch_35_batchsize_2.hdf5'
#modelpath = './models_best/r2unet_model_1920x1056_epoch_25_batchsize_2.hdf5'

height = 1056#720
width = 1920#1280

thresh = 0.5
net = 3  # 0:vgg, 1:mobilenet, 2:wcid, 3:attunet,r2unet


pths_img = sorted(glob(os.path.join(dataset_path, 'test/*.jpg')))
pths_GT = sorted(glob(os.path.join(dataset_path, 'test_GT/*.png')))


# load model:
if net < 2:
    width = 1920
    height = 1056
    if net == 0:
        from keras_segmentation.models.unet import vgg_unet
        modelpath = './models_best/vgg_unet/epochs.00058'
        model = vgg_unet(n_classes=3, input_height=height, input_width=width)
    else:
        from keras_segmentation.models.segnet import mobilenet_segnet
        modelpath = './models_best/mobilenet/epochs.00006'
        model = mobilenet_segnet(n_classes=3, input_height=height, input_width=width)
    
    model.load_weights(modelpath)

elif net>=2:  # attunet, r2unet, wcid
    import tensorflow as tf
    if net == 2:
        width = 1920
        height = 1056
        modelpath = './models_best/checkpoint_076.hdf5'
    model = tf.keras.models.load_model(modelpath, compile=False)


# prediction:
f1 = []
iou = []
accuracy = []
precision = []
recall = []
while len(pths_img) != 0:
    # get next image:
    img = cv2.imread(pths_GT.pop())
    y_true = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.imread(pths_img.pop())

    # skip empty GTs:
    if not np.any(y_true):
        continue

    img = cv2.resize(img, tuple([width, height]), interpolation=cv2.INTER_LINEAR)

    if net < 2:
        y_pred = model.predict_segmentation(inp=img, prob_seg_arr=True, resize=True)

    elif net>=2:  # attunet, r2unet, wcid
        img = np.expand_dims(img, axis=0)
        y_pred = model.predict(img/255.)
        if net == 2:
            y_pred = np.squeeze(y_pred, axis=(0, 3))
        else:
            y_pred = 1 - y_pred[0,:,:,0]

    y_pred = cv2.resize(y_pred, tuple([y_true.shape[1], y_true.shape[0]]), interpolation=cv2.INTER_LINEAR) > thresh
    y_pred = y_pred.flatten()
    y_true = y_true.flatten()

    # Berechnung von F1-Score und IoU
    f1.append(f1_score(y_true, y_pred))
    iou.append(jaccard_score(y_true, y_pred))
    accuracy.append(accuracy_score(y_true, y_pred))
    precision.append(precision_score(y_true, y_pred))
    recall.append(recall_score(y_true, y_pred))

print(f"F1-Score: {np.mean(f1)}")
print(f"IoU: {np.mean(iou)}")
print(f"Accuracy: {np.mean(accuracy)}")
print(f"Precision: {np.mean(precision)}")
print(f"Recall: {np.mean(recall)}")

