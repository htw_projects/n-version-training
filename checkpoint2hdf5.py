# this script converts a checkpoint into an hdf5-file


from collection.keras_unet_collection import models

height = 1056
width = 1920

#model = models.r2_unet_2d((height, width, 3), [32, 64, 128, 256, 512, 1024], 
#                          dropout=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], n_labels=2,
#                          stack_num_down=2, stack_num_up=2, recur_num=2,
#                          activation='ReLU', output_activation='Sigmoid', 
#                          batch_norm=True, pool=True, unpool=True, name='r2unet')

model = models.att_unet_2d((height, width, 3), filter_num=[64, 128, 256, 512, 1024], 
                        dropout=[0.5, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.5], n_labels=2,
                        stack_num_down=2, stack_num_up=2, activation='ReLU',
                        atten_activation='ReLU', attention='add', output_activation='Sigmoid',
                        batch_norm=True, pool=True, unpool=True,
                        backbone='VGG16', #weights='imagenet',
                        freeze_backbone=False, freeze_batch_norm=False,
                        name='attunet')

checkpoint_path = "./checkpoints/r2unet_model.25"
checkpoint_path = "./checkpoints/attunet_model.22"
model.load_weights(checkpoint_path)

#model.save(f'r2unet_model_1920x1056_epoch_25_batchsize_2.hdf5')
model.save(f'attunet_model_1920x1056_epoch_22_batchsize_4x8_drop_0p5x1.hdf5')
