"""

Autor: Biranavan Parameswaran
Repository: https://gitlab.rz.htw-berlin.de/se_perception/n-version/-/blob/n-version-biri-abschlussarbeit/scripts/create_dataset_single_class/split_dataset_single_class.py
Modified by Christopher Borchardt

This Dataset splits the Railsem19 dataset into train , valid and test.
You can now set your own classes by listing them in the input of this programm:

python split_dataset.py --class_mapping "1: rail-track, rail-raised; 2: trackbed" --save_path /my/savepath --dataset /my/dataset

This command will map the classes rail-track and rail-raised to 1 and trackbed to 2. All remaining classes wil be mapped to 0.
By default the class mapping is "1: rail-track, rail-raised"
The class numbers represent the pixel value in the uint8 encoded mask.
The overall ratio is 70% train, 20% validation and 10% test.

Please use it on the original dataset.
"""
import shutil
import argparse
import math
import random
from pathlib import Path
import cv2
import numpy as np
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import json
import os

def save_mask(path:str,msk:np.ndarray):
    cv2.imwrite(path,msk)
def save_symlink(old_path,new_path):
    os.symlink(old_path,new_path)

def normal_round(n):
    if n - math.floor(n) < 0.5:
        return math.floor(n)
    return math.ceil(n)

def get_class_index(json_file: str, class_name: str) -> int:
    with open(json_file, 'r') as f:
        data = json.load(f)
        
    for i, label in enumerate(data['labels']):
        if label['name'] == class_name:
            return i
    
    raise ValueError(f"Class {class_name} not found in {json_file}")
def validate_mask(mask_original: np.ndarray, mask_changed: np.ndarray, class_indices:dict) -> bool:#rail_track_index: int, rail_raised_index: int) -> bool:
    """
    Validate if the mask was changed correctly.
    """

    # Check if the masks have the same shape
    if mask_original.shape != mask_changed.shape:
        msg = ("The masks do not have the same shape.")
        return False,msg

    # check every target in class_indices
    for target_class, original_classes in class_indices.items():
        for original_class in original_classes:
            # Get all the indices where mask_original has value of original_class
            original_class_indices = np.where(mask_original == original_class)

            # Check if these indices are set to the target_class value in mask_changed
            if not np.all(mask_changed[original_class_indices] == target_class):
                msg = f"Some indices of original class {original_class} are not correctly mapped to target class {target_class} in the changed mask."
                return False, msg

    # Get all indices that are not in any of the original classes
    all_original_classes = [idx for sublist in class_indices.values() for idx in sublist]
    other_indices = np.where(~np.isin(mask_original, all_original_classes))

    # Check if all these indices are set to 0 in mask_changed
    if not np.all(mask_changed[other_indices] == 0):
        msg = "Some indices not belonging to any original class are not correctly mapped to 0 in the changed mask."
        return False, msg

    # If all checks passed, return True
    return True,"passed"


def split_data(save_path:str, dataset:str, class_mapping:dict, train_size_perc=0.7,valid_size_perc=0.2,test_size_perc=0.1,test_size_bg=0.05):
    # create temp dir
    if (save_path=="./tmp") or dataset=="./tmp":
        raise Exception("Please dont use ./tmp as path")
    tmp_path = Path("./tmp")
    bg_path = (tmp_path / "bg")
    non_bg_path = (tmp_path/"non_bg")
        
    
    if not tmp_path.exists():
        tmp_path.mkdir()
        
    if not bg_path.exists():
        bg_path.mkdir()
    if not non_bg_path.exists():
        non_bg_path.mkdir()
            
    
    # Check and create folders
    save_path = Path(save_path)  
    if not save_path.exists():
        save_path.mkdir()
    for subset in ["train", "valid", "test"]:
        if not (save_path / subset).exists():
            (save_path / subset).mkdir()
        if not (save_path / f"{subset}_GT").exists():
            (save_path / f"{subset}_GT").mkdir()
    img_path =dataset+ "/jpgs/rs19_val"
    json_path = dataset + "/rs19-config.json"
    images = list(Path(img_path).iterdir())
   
    # get index of rail-track and rail-raised classes
    class_indices = {}
    for index, classes in class_mapping.items():
        class_indices[index] = [get_class_index(json_path, class_json) for class_json in classes]

    img_non_bg_data = {}
    img_bg_data= {}
    
    for img in tqdm(images, desc="Processing images and validating changed mask"):
        msk_path = str(img).replace("jpgs", "uint8").replace(".jpg", ".png")
        msk_original = cv2.imread(msk_path, cv2.IMREAD_GRAYSCALE)
        msk = np.zeros_like(msk_original)

        # Convert all original classes to their respective target classes
        for target, original in class_indices.items():
            msk[np.isin(msk_original, original)] = target

        key = img.stem
        verified,msg = validate_mask(mask_changed=msk, mask_original=msk_original,
                                        class_indices=class_indices)#rail_raised_index=rail_raised_index, rail_track_index=rail_track_index)    
        if not verified:
                raise ValueError(f"Image {img} failed validation. Reason: {msg}")

        # check if msk is a background image then save it to bg folder
        if np.all(msk == 0):
            save_mask_path = str(bg_path.joinpath(f"{img.name}.png"))
            save_mask(save_mask_path,msk)  
            img_bg_data[key] = {
                "msk_path":save_mask_path,
                "img_path": str(img)
            }
        else:
            # save the mask to non_bg folder
            save_mask_path = str(non_bg_path.joinpath(f"{img.name}.png"))
            save_mask(save_mask_path,msk)  
            img_non_bg_data[key] = {
                "msk_path":save_mask_path,
                "img_path": str(img)
            }
    
    images_bg = list(img_bg_data.keys())
    images_non_bg = list(img_non_bg_data.keys())
    
    # shuffle the images
    random.shuffle(images_bg)
    random.shuffle(images_non_bg)
    
    # Total number of background images
    background_imgs_size = len(images_bg)

    # Total number of non-background images
    non_background_imgs_size = len(images_non_bg)
    
    # Total number of images
    total_imgs = background_imgs_size + non_background_imgs_size

    print(f"background images: {background_imgs_size}")
    print(f"non- background images: {non_background_imgs_size}")
    print(f"total: {total_imgs}")

    total_test_size = normal_round(total_imgs * test_size_perc)

    # Calculate the number of background and non-background images in the test set
    back_ground_test_size = normal_round(background_imgs_size * test_size_bg)  # Around 10% of background images for testing
    non_back_ground_test_size = total_test_size - back_ground_test_size  # The rest of the test set will be non-background images

    # Split the images
    test_bg = images_bg[:back_ground_test_size]
    images_bg = images_bg[back_ground_test_size:]

    test_img = images_non_bg[:non_back_ground_test_size]
    images_non_bg = images_non_bg[non_back_ground_test_size:]

    # Calculate the sizes for training and validation sets
    background_train_size = normal_round(len(images_bg) * train_size_perc / (train_size_perc + valid_size_perc))

    non_background_train_size = normal_round(len(images_non_bg) * train_size_perc / (train_size_perc + valid_size_perc))

    # Split the remaining images into training and validation sets
    train_bg, valid_bg = train_test_split(images_bg, train_size=background_train_size, random_state=42)
    train_img, valid_img = train_test_split(images_non_bg, train_size=non_background_train_size, random_state=42)
    

    # Combine the splits
    train_img.extend(train_bg)
    valid_img.extend(valid_bg)
    test_img.extend(test_bg)
    
    
    # distribute the images to the respective folders and save	
    for subset,images in [("train",train_img),("valid",valid_img),("test",test_img)]:
        for img_name in tqdm(images, desc=f"saving {subset} data"):
            if img_name in img_bg_data:
               msk_path = img_bg_data[img_name]["msk_path"]
               img_path = img_bg_data[img_name]["img_path"]
            else:
                msk_path = img_non_bg_data[img_name]["msk_path"]
                img_path = img_non_bg_data[img_name]["img_path"]
                
            annotations_path = f"{save_path}/{subset}_GT/{img_name}.png"
            img_save_path = f"{save_path}/{subset}/{img_name}.jpg"
            shutil.move(msk_path,annotations_path)
            shutil.copyfile(img_path,img_save_path)
            
    # remove the temporary folder
    shutil.rmtree(str(tmp_path))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_path", type=str, help="Path to save the data")
    parser.add_argument("--dataset", type=str, help="Path to the Railsem dataset")
    parser.add_argument("--class_mapping", type=str, default="1: rail-track, rail-raised", help="Mapping of dataset classes to new classes, e.g., '1: class1,class2; 2: class3,class4'")
    parser.add_argument("--train_size", type=float, default=70.0, help="Size of training subset in percent")
    parser.add_argument("--valid_size", type=float, default=20.0, help="Size of validation subset in percent")
    parser.add_argument("--test_size", type=float, default=10.0, help="Size of testing subset in percent")
    parser.add_argument("--bg_size", type=float, default=10.0, help="Size of testing background images in percent")


    args = parser.parse_args()
    
    save_path = (args.save_path)  
    dataset = (args.dataset)
    class_mapping = {}
    for item in args.class_mapping.split(';'):
        key, value = item.split(':')
        class_mapping[int(key)] = [v.strip() for v in value.split(',')]


    split_data(save_path, dataset, class_mapping, 
               train_size_perc=args.train_size/100, 
               valid_size_perc=args.valid_size/100, 
               test_size_perc=args.test_size/100,
               test_size_bg=args.bg_size/100)
   
    





